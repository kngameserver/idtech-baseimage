# Throwaway build stage
FROM debian:buster-slim AS build

# Add i386 architecture support
RUN dpkg --add-architecture i386

# Install dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates=20200601~deb10u2 \
    libstdc++5:i386=1:3.3.6-30 \
    libc6-i386 \
    lib32z1 \
    zlib1g:i386 \
    g++-multilib=4:8.3.0-1 

FROM alpine:latest

LABEL maintainer="fistwho@keinnerd.net"

# Install dependencies
RUN apk add --no-cache wget bash ca-certificates tzdata

# Copy needed libs from build stage
COPY --from=build /usr/lib/i386-linux-gnu/ /usr/lib/i386-linux-gnu/
COPY --from=build /lib/i386-linux-gnu/ /lib/i386-linux-gnu/
COPY --from=build /lib/ld-linux.so.2 /lib/ld-linux.so.2

# Add User an needed folders
RUN mkdir \
    /gameserver \
    /config
